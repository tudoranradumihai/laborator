<?php
session_start();

if (array_key_exists("HTTP_REFERER",$_SERVER)){
	if (preg_match("/confirm.php?/", $_SERVER["HTTP_REFERER"])){
		if (!file_exists("archive")){
			mkdir("archive");
		}
		$temporary = scandir("archive");
		foreach ($temporary as $key => $value) {
			if (in_array($value,array(".",".."))){
				unset($temporary[$key]);
			}
		}
		$count = count($temporary);
		$count++;
		$handler = fopen("archive/".$count.".txt","a+");
		fwrite($handler,json_encode($_SESSION["data"]));
		fclose($handler);
	} else {
		echo '<a href="index.php">NASPA, AIUREA, CAM NASOL</a>';
	}
} else {
	echo '<a href="index.php">NASPA, AIUREA, CAM NASOL</a>';
}
?>